From ad959c7789eb856575cd6300c7e96b6a8ba1bdc2 Mon Sep 17 00:00:00 2001
From: Ryan Prichard <rprichard@google.com>
Date: Tue, 10 Dec 2024 16:16:15 -0800
Subject: [PATCH] ELF: Have __rela_iplt_{start,end} surround .rela.iplt with
 --pack-dyn-relocs=android.

In #86751 we moved the IRELATIVE relocations to .rela.plt when
--pack-dyn-relocs=android was enabled but we neglected to also move
the __rela_iplt_{start,end} symbols. As a result, static binaries
linked with this flag were unable to find their IRELATIVE relocations.
Fix it by having the symbols surround the correct section.

Reviewers: MaskRay, smithp35

Reviewed By: MaskRay

Pull Request: https://github.com/llvm/llvm-project/pull/118585

Change-Id: I27dd238b1f87961e0175670594a8b50770d94432
---
 lld/ELF/Relocations.cpp              | 21 ++++++++++++---------
 lld/ELF/Relocations.h                |  3 +++
 lld/ELF/Writer.cpp                   | 14 +++++++++-----
 lld/test/ELF/pack-dyn-relocs-ifunc.s | 25 +++++++++++++++++++++++++
 4 files changed, 49 insertions(+), 14 deletions(-)

diff --git a/lld/ELF/Relocations.cpp b/lld/ELF/Relocations.cpp
index e5f58f1a7dd1..f7d37d70345d 100644
--- a/lld/ELF/Relocations.cpp
+++ b/lld/ELF/Relocations.cpp
@@ -1673,6 +1673,17 @@ template <class ELFT> void elf::scanRelocations() {
   });
 }
 
+RelocationBaseSection &elf::getIRelativeSection() {
+  // Prior to Android V, there was a bug that caused RELR relocations to be
+  // applied after packed relocations. This meant that resolvers referenced by
+  // IRELATIVE relocations in the packed relocation section would read
+  // unrelocated globals with RELR relocations when
+  // --pack-relative-relocs=android+relr is enabled. Work around this by placing
+  // IRELATIVE in .rela.plt.
+  return config->androidPackDynRelocs ? *in.relaPlt
+                                      : *ctx.mainPart->relaDyn;
+}
+
 static bool handleNonPreemptibleIfunc(Symbol &sym, uint16_t flags) {
   // Handle a reference to a non-preemptible ifunc. These are special in a
   // few ways:
@@ -1722,17 +1733,9 @@ static bool handleNonPreemptibleIfunc(Symbol &sym, uint16_t flags) {
   // original section/value pairs. For non-GOT non-PLT relocation case below, we
   // may alter section/value, so create a copy of the symbol to make
   // section/value fixed.
-  //
-  // Prior to Android V, there was a bug that caused RELR relocations to be
-  // applied after packed relocations. This meant that resolvers referenced by
-  // IRELATIVE relocations in the packed relocation section would read
-  // unrelocated globals with RELR relocations when
-  // --pack-relative-relocs=android+relr is enabled. Work around this by placing
-  // IRELATIVE in .rela.plt.
   auto *directSym = makeDefined(cast<Defined>(sym));
   directSym->allocateAux();
-  auto &dyn =
-      config->androidPackDynRelocs ? *in.relaPlt : *ctx.mainPart->relaDyn;
+  auto &dyn = getIRelativeSection();
   addPltEntry(*in.iplt, *in.igotPlt, dyn, ctx.target->iRelativeRel, *directSym);
   sym.allocateAux();
   ctx.symAux.back().pltIdx = ctx.symAux[directSym->auxIdx].pltIdx;
diff --git a/lld/ELF/Relocations.h b/lld/ELF/Relocations.h
index aaa4581490a2..176a2058babf 100644
--- a/lld/ELF/Relocations.h
+++ b/lld/ELF/Relocations.h
@@ -20,6 +20,7 @@ class Symbol;
 class InputSection;
 class InputSectionBase;
 class OutputSection;
+class RelocationBaseSection;
 class SectionBase;
 
 // Represents a relocation type, such as R_X86_64_PC32 or R_ARM_THM_CALL.
@@ -327,6 +328,8 @@ sortRels(Relocs<llvm::object::Elf_Crel_Impl<is64>> rels,
   return {};
 }
 
+RelocationBaseSection &getIRelativeSection();
+
 // Returns true if Expr refers a GOT entry. Note that this function returns
 // false for TLS variables even though they need GOT, because TLS variables uses
 // GOT differently than the regular variables.
diff --git a/lld/ELF/Writer.cpp b/lld/ELF/Writer.cpp
index 016525355171..3ee13b71a612 100644
--- a/lld/ELF/Writer.cpp
+++ b/lld/ELF/Writer.cpp
@@ -832,11 +832,15 @@ template <class ELFT> void Writer<ELFT>::setReservedSymbolSections() {
     ctx.sym.globalOffsetTable->section = sec;
   }
 
-  // .rela_iplt_{start,end} mark the start and the end of .rel[a].dyn.
-  if (ctx.sym.relaIpltStart && ctx.mainPart->relaDyn->isNeeded()) {
-    ctx.sym.relaIpltStart->section = ctx.mainPart->relaDyn.get();
-    ctx.sym.relaIpltEnd->section = ctx.mainPart->relaDyn.get();
-    ctx.sym.relaIpltEnd->value = ctx.mainPart->relaDyn->getSize();
+  // .rela_iplt_{start,end} mark the start and the end of the section containing
+  // IRELATIVE relocations.
+  if (ctx.sym.relaIpltStart) {
+    auto &dyn = getIRelativeSection();
+    if (dyn.isNeeded()) {
+      ctx.sym.relaIpltStart->section = &dyn;
+      ctx.sym.relaIpltEnd->section = &dyn;
+      ctx.sym.relaIpltEnd->value = dyn.getSize();
+    }
   }
 
   PhdrEntry *last = nullptr;
diff --git a/lld/test/ELF/pack-dyn-relocs-ifunc.s b/lld/test/ELF/pack-dyn-relocs-ifunc.s
index 6168d06f99d9..b6000df3d616 100644
--- a/lld/test/ELF/pack-dyn-relocs-ifunc.s
+++ b/lld/test/ELF/pack-dyn-relocs-ifunc.s
@@ -47,3 +47,28 @@ _start:
 .globl bar
 bar:
   ret
+
+#--- c.s
+
+# RUN: llvm-mc -filetype=obj -triple=aarch64-linux-android c.s -o c.o
+# RUN: ld.lld --pack-dyn-relocs=android c.o -o c
+# RUN: llvm-readelf -sS c | FileCheck --check-prefix=STATIC %s
+
+# STATIC: .rela.plt         RELA            0000000000200158 000158 000018 18  AI  0   5  8
+# STATIC: 0000000000200158     0 NOTYPE  LOCAL  HIDDEN      1 __rela_iplt_start
+# STATIC: 0000000000200170     0 NOTYPE  LOCAL  HIDDEN      1 __rela_iplt_end
+
+.text
+.type foo, %gnu_indirect_function
+.globl foo
+foo:
+  ret
+
+.globl _start
+_start:
+  bl foo
+
+.data
+.balign 8
+.quad __rela_iplt_start
+.quad __rela_iplt_end
-- 
2.47.1.545.g3c1d2e2a6a-goog

